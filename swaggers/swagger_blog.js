/**
 * @swagger
 * /v1/blogs?page_number={page_number}&page_count={page_count}&sort_on={sort_on}&sort_by={sort_by}:
 *  get:
 *      tags:
 *        - BLOGS
 *      summary: Resturns blogs
 *      description: List of blogs
 *      produces:
 *          application/json
 *      parameters:
 *       - name: page_number
 *         in: path
 *         required: true
 *         type: integer
 *         example: 0
 *       - name: page_count
 *         in: path
 *         required: true
 *         type: integer
 *         example: 10
 *       - name: sort_on
 *         in: path
 *         required: true
 *         type: string
 *         example: id
 *       - name: sort_by
 *         in: path
 *         required: true
 *         type: string
 *         example: desc
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Failed.
 */

/**
 * @swagger
 * /v1/blogs/byAuthor/{id}?page_number={page_number}&page_count={page_count}&sort_on={sort_on}&sort_by={sort_by}:
 *  get:
 *      tags:
 *        - BLOGS
 *      summary: Resturns blogs by auther id
 *      description: List of blogs
 *      produces:
 *          application/json
 *      parameters:
 *       - name: page_number
 *         in: path
 *         required: true
 *         type: integer
 *         example: 0
 *       - name: page_count
 *         in: path
 *         required: true
 *         type: integer
 *         example: 10
 *       - name: sort_on
 *         in: path
 *         required: true
 *         type: string
 *         example: id
 *       - name: sort_by
 *         in: path
 *         required: true
 *         type: string
 *         example: desc
 *       - name: id
 *         in: path
 *         required: true
 *         type: integer
 *         example: author id
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Failed.
 */

/**
 * @swagger
 * definitions:
 *  Blog:
 *      type: object
 *      required:
 *          - title
 *      properties:
 *          title:
 *              type: string
 *              required: true
 *              example: 'First Blog'
 *          description:
 *              type: string
 *              required: false
 *              example: 'Test Description'
 *          author:
 *              type: integer
 *              required: true
 *              example: 1
 */

/**
 * @swagger
 * /v1/blogs/:
 *  post:
 *      tags:
 *          - BLOGS
 *      summary: Create Blogs
 *      description: Create new blog
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: body
 *            name: request
 *            required: true
 *            example: Request body
 *            schema:
 *              $ref: '#/definitions/Blog'
 *      responses:
 *          201:
 *              description: Created.
 *              content:
 *                  application/json
 *          500:
 *              description: Failed.
 */

/**
 * @swagger
 * /v1/blogs/{id}:
 *  put:
 *      tags:
 *          - BLOGS
 *      summary: Update Blog
 *      description: Update blog
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            example: blog id
 *          - in: body
 *            name: request
 *            required: true
 *            example: Request body
 *            schema:
 *              $ref: '#/definitions/Blog'
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Failed.
 */

/**
 * @swagger
 * /v1/blogs/{id}:
 *  delete:
 *      tags:
 *          - BLOGS
 *      summary: Delete Blog
 *      description: Delete blog
 *      consumes:
 *          - application/json
 *      parameters:
 *          - in: path
 *            name: id
 *            required: true
 *            example: blog id
 *      responses:
 *          200:
 *              description: Success.
 *              content:
 *                  application/json
 *          500:
 *              description: Failed.
 */