const nodemailer = require('nodemailer');
const pool = require(`../lib/mysql`);


const sendEmail = async (id, emails) => {
    try {
        var transporter = nodemailer.createTransport({
            service: process.env.EMAIL_SERVICE,
            auth: {
                user: process.env.EMAIL_ID,
                pass: process.env.EMAIL_PASSWORD
            }
        });

        var mailOptions = {
            from: process.env.EMAIL_ID,
            to: emails.toString(),
            subject: 'Email Notification',
            html: '<h1>Welcome</h1><p>New blog created!</p>'
        };

        transporter.sendMail(mailOptions, async (error, info) => {
            if (error) {
                console.log(error);
            } else {
                const emailSentStatus = await pool.query(`update blogs set email_sent_status = ? where id = ?`, [true, id]);
                console.log(info.response, id);
            }
        });
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

module.exports = { sendEmail };