const pool = require(`../lib/mysql`);
const emailService = require(`../services/emailService`);

const blogs = async ({ page_number, page_count, sort_by, sort_on }) => {
    try {
        const blogs = await pool.query(`select b.id, b.title, b.description, a.name "author" from blogs b left join authors a on b.author = a.id order by b.${sort_on} ${sort_by} limit ? offset ?`, [+page_count, (+page_number * +page_count)]);
        return { status: 200, message: (blogs[0].length > 0) ? `data found` : `no records found`, data: blogs[0] };
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

const blogsByAuthor = async ({ id }, { page_number, page_count, sort_by, sort_on }) => {
    try {
        const blogs = await pool.query(`select b.id, b.title, b.description, a.name "author" from blogs b left join authors a on b.author = a.id where author = ? order by b.${sort_on} ${sort_by} limit ? offset ?`, [id, +page_count, (+page_number * +page_count)]);
        return { status: 200, message: (blogs[0].length > 0) ? `data found` : `no records found`, data: blogs[0] };
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

const createBlog = async ({ title, description, author }) => {
    try {
        const blog = await pool.query(`insert into blogs (title, description, author, created_by, created_at, updated_by, updated_at, email_sent_status) value (?, ?, ?, ?, now(), ?, ?, ?)`, [title, description, author, `system`, null, null, false]);

        const emails = await pool.query(`select json_arrayagg(email) as emails from authors where id != ?`, [author]);
        const sentStatus = await emailService.sendEmail(blog[0].insertId, emails[0][0].emails);

        return { status: 201, message: `data inserted`, data: blog[0].insertId, emailSentStatus: true };
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

const editBlog = async ({ id }, { title, description, author }) => {
    try {
        const blog = await pool.query(`update blogs set title = ?, description = ?, author = ?, updated_by = ?, updated_at = now() where id = ?`, [title, description, author, `system`, id]);
        return { status: 200, message: `data updated`, data: [] };
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

const deleteBlog = async ({ id }) => {
    try {
        const blog = await pool.query(`delete from blogs where id = ?`, [id]);
        return { status: 200, message: `data deleted`, data: [] };
    } catch (error) {
        throw { status: 400, message: error.message };
    }
}

module.exports = { blogs, createBlog, editBlog, deleteBlog, blogsByAuthor };