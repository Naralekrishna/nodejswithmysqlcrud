var express = require('express');
var router = express.Router();

const blogService = require(`../../services/blogService`);

router.get('/', async (req, res, next) => {
    try {
        const blogs = await blogService.blogs(req.query);
        res.status(blogs.status).json(blogs);
    } catch (error) {
        res.status(error.status).json({ status: error.status, message: error.message });
    }
});

router.get('/byAuthor/:id', async (req, res, next) => {
    try {
        const blogs = await blogService.blogsByAuthor(req.params, req.query);
        res.status(blogs.status).json(blogs);
    } catch (error) {
        res.status(error.status).json({ status: error.status, message: error.message });
    }
});

router.post('/', async (req, res, next) => {
    try {
        const blog = await blogService.createBlog(req.body);
        res.status(blog.status).json(blog);
    } catch (error) {
        res.status(error.status).json({ status: error.status, message: error.message });
    }
});

router.put('/:id', async (req, res, next) => {
    try {
        const blog = await blogService.editBlog(req.params, req.body);
        res.status(blog.status).json(blog);
    } catch (error) {
        res.status(error.status).json({ status: error.status, message: error.message });
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
        const blog = await blogService.deleteBlog(req.params);
        res.status(blog.status).json(blog);
    } catch (error) {
        res.status(error.status).json({ status: error.status, message: error.message });
    }
});

module.exports = router;